package com.cupofcrumley;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CrumleyDemoApplicationTests {

	@Test
	public void contextLoads() {
		// Nullop test which loads the spring context and serves as a sanity check.
	}

}
