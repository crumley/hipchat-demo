package com.cupofcrumley.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.cupofcrumley.summary.ImmutableMessageEvent;
import com.cupofcrumley.summary.MessageEventSummary;
import com.cupofcrumley.summary.rest.MessageSummaryClient;
import com.cupofcrumley.summary.rest.MessageSummaryEndpoint;
import com.google.common.collect.Lists;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Api level test cases for {@link MessageSummaryEndpoint}. Starts an instance of the
 * server and uses real http requests.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MessageSummaryApiTest {
	@LocalServerPort
	int port;

	private MessageSummaryClient client;

	@Before
	public void init() {
		// Use Retrofit to construct a client interface for interacting with the 
		// local API server.
		client = new Retrofit.Builder()
					.addConverterFactory(JacksonConverterFactory.create())
					.baseUrl("http://localhost:" + port)
					.build()
					.create(MessageSummaryClient.class);
	}

	/**
	 * Example 1 from the problem description.
	 */
	@Test
	public void testSimpleMention() {
		MessageEventSummary response = sendMessaage("@chris you around?");

		// Ensure emoticons and links were NOT detected
		assertEmoticonsAreEmpty(response);
		assertLinksAreEmpty(response);

		// Ensure that chris was detected
		assertThat(response.mentions())
			.isNotNull()
			.containsExactly("chris");
	}

	private void assertEmoticonsAreEmpty(MessageEventSummary response) {
		assertThat(response.emoticons()).isNotNull().isEmpty();
	}

	private void assertLinksAreEmpty(MessageEventSummary response) {
		assertThat(response.links()).isNotNull().isEmpty();
	}

	/**
	 * Sends a message (string) to the API server. 
	 */
	private MessageEventSummary sendMessaage(String message) {
		ImmutableMessageEvent messageEvent = ImmutableMessageEvent.of(message, Lists.newArrayList());
		Call<MessageEventSummary> request = client.sendMessage(messageEvent);
		Response<MessageEventSummary> response;
		try {
			response = request.execute();
			assertThat(response.isSuccessful()).isTrue();

			MessageEventSummary body = response.body();
			assertThat(body).isNotNull();
			return body;
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

}
