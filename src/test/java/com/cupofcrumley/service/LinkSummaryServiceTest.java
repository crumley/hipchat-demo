package com.cupofcrumley.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.cupofcrumley.summary.ImmutableUrlSummary;
import com.cupofcrumley.summary.MessageEvent;
import com.cupofcrumley.summary.UrlSummary;
import com.cupofcrumley.summary.service.LinkSummaryGenerator;
import com.cupofcrumley.summary.service.LinkSummaryService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LinkSummaryServiceTest {
	private static final String[] TEST_DATA = {"http://a", "https://b", "nonurl", "ftp://a"};
	
	@Autowired
    private LinkSummaryService summaryService;
	
	@MockBean
    private LinkSummaryGenerator linkSummaryGenerator;
	
	@Captor
    private ArgumentCaptor<String> urlCapture;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSimpleMention() {
		ImmutableUrlSummary summary = ImmutableUrlSummary.of("http://test-url", "Test Title");
		given(linkSummaryGenerator.getSummary(urlCapture.capture())).willReturn(Optional.of(summary));
		
		// Dispatch a message with several URLs and non urls. 
		MessageEvent event = MessageEvent.withBody(String.join(" ", TEST_DATA));
		List<UrlSummary> summaries = summaryService.getSummary(event);
		
		// Ensure two urls were passed to the linkSummaryGenerator
		assertThat(urlCapture.getAllValues())
        	.containsExactly(TEST_DATA[0], TEST_DATA[1]);
		
		// Ensure two url summaries were returned
		assertThat(summaries)
			.hasSize(2);
		
		// TODO Expand, test with real html, etc.
	}
}
