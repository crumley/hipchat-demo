package com.cupofcrumley;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrumleyDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrumleyDemoApplication.class, args);
	}
}
