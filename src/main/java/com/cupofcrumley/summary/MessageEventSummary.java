package com.cupofcrumley.summary;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Structure which represents the summary of a MessageEvent. 
 */
@Value.Immutable
@JsonSerialize(as = ImmutableMessageEventSummary.class)
@JsonDeserialize(as = ImmutableMessageEventSummary.class)
public interface MessageEventSummary {
	List<String> mentions();
	List<String> emoticons();
	List<UrlSummary> links();
}
