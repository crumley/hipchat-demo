/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cupofcrumley.summary.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cupofcrumley.summary.ImmutableMessageEventSummary;
import com.cupofcrumley.summary.MessageEvent;
import com.cupofcrumley.summary.MessageEventSummary;
import com.cupofcrumley.summary.UrlSummary;
import com.cupofcrumley.summary.service.LinkSummaryService;



/*
 * Rest endpoint for the message summary service.
 */
@RestController
public class MessageSummaryEndpoint {
	private static final Logger logger = LoggerFactory.getLogger(MessageSummaryEndpoint.class);

	@Autowired
	private LinkSummaryService linkSummaryService;
	
	@PostMapping(path = "/messages/")
	public MessageEventSummary messageEvent(@RequestBody MessageEvent event) {
		logger.info("message.post event={}", event);
		
		List<UrlSummary> links = linkSummaryService.getSummary(event);
		
		// TODO Extend for mentions and emoticons. Initially as synchronous operations for simplicity but most likely
		// would best be implemented asynchronously and in parallel as these operations don't depend on each other.
		
		return ImmutableMessageEventSummary.builder()
					.addMentions("chris")
					.addAllLinks(links)
					.build();
	}

}
