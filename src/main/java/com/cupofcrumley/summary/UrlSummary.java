package com.cupofcrumley.summary;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Structure which represents the summary of a url (typically discovered in the body of a MessageEvent).
 */
@Value.Immutable
@JsonSerialize(as = ImmutableUrlSummary.class)
@JsonDeserialize(as = ImmutableUrlSummary.class)
public interface UrlSummary {
	@Value.Parameter
	String url();
	
	@Value.Parameter
	String title();
}