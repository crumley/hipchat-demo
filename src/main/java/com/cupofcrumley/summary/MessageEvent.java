package com.cupofcrumley.summary;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/*
 * Structure used to represent a new messages posted to one or more channels. 
 */
@Value.Immutable
@JsonSerialize(as = ImmutableMessageEvent.class)
@JsonDeserialize(as = ImmutableMessageEvent.class)
public interface MessageEvent {
	@Value.Parameter
	String body();
	
	@Value.Parameter
	List<String> channels();
	
	public static MessageEvent withBody(String body) {
		return ImmutableMessageEvent.builder()
				.body(body)
				.build();
	}
}
