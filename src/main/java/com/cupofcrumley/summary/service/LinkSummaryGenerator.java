package com.cupofcrumley.summary.service;

import java.util.Optional;

import com.cupofcrumley.summary.UrlSummary;

/**
 * Converts a url to a {@link UrlSummary}.
 */
public interface LinkSummaryGenerator {

	Optional<UrlSummary> getSummary(String url);

}