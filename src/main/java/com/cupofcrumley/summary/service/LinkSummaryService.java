package com.cupofcrumley.summary.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cupofcrumley.summary.MessageEvent;
import com.cupofcrumley.summary.UrlSummary;

/**
 * Responsible for converting {@link MessageEvent} into one or more {@link UrlSummary}
 * using a {@link LinkSummaryGenerator}.
 */
@Service
public class LinkSummaryService {
	@Autowired
	private LinkSummaryGenerator summaryGenerator;
	
	public List<UrlSummary> getSummary(MessageEvent message) {
		// This code makes assumptions in the name of simplifying the code. For instance 
		// `summaryGenerator::getSummary` typically would throw an exception and allow
		// this caller to gracefully fallback on a default title (or signal to the client the url was detected but no title was available). 
		// Instead this implementation returns an Optional.empty() and the url will appear like it was not detected. This 
		// is intentional but should be addressed before this feature can be considered "done". 
		return toWordStream(message)
				.filter(word -> isUrl(word))
				.map(summaryGenerator::getSummary)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toList());
	}

	/**
	 * Create a stream of words by splitting on space. While this appears to work for this demo
	 * (urls can not contain spaces) this should be reviewed in more depth.   
	 */
	private Stream<String> toWordStream(MessageEvent message) {
		return Stream.of(message.body())
				.map(body -> body.split(" "))
				.flatMap(Arrays::stream);
	}
	
	/**
	 * Return true if the url is http or https.
	 */
	private static boolean isUrl(String url) {
		return url.startsWith("http://") || url.startsWith("https://"); 
	}
}
