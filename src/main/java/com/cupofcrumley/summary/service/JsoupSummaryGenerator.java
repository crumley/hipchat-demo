package com.cupofcrumley.summary.service;

import java.io.IOException;
import java.util.Optional;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cupofcrumley.summary.ImmutableUrlSummary;
import com.cupofcrumley.summary.UrlSummary;

/**
 * Generates {@link UrlSummary} objects using Jsoup. Jsoup is a HTML parsing library
 * that is forgiving of poorly formatted markup (and other hazards found on the www). 
 */
@Service
public class JsoupSummaryGenerator implements LinkSummaryGenerator {
	private static final Logger logger = LoggerFactory.getLogger(JsoupSummaryGenerator.class);
	
	@Override
	public Optional<UrlSummary> getSummary(String url) {
		String title;
		try {
			// Make sure to set a timeout. In this case 3s is the connect and read timeout. Future implementations 
			// may want to implement retry with backoff, asynchronous processing, etc.
			title = Jsoup.connect(url).timeout(3000).get().title();
		} catch (IOException e) {
			// Log failure metric
			return Optional.empty();
		}
		
		logger.debug("link.summary.jsoup url={} title={}", url, title);
		
		return Optional.of(ImmutableUrlSummary.of(url, title));
	}
	
}
