
# Ryan Crumley's HipChat Demo Project

This is an implementation of the HipChat problem statement.

## Technology

- The project was created using Java/Maven/Spring Boot. This stack was chosen based on the time
constraints and the resources available (Ryan =)).

- Why Java? I am familiar with golang but I am more familiar with the java ecosystem. I
used Java to produce a more interesting project in the allotted time.

- Why Maven? I selected Maven because I knew I could get a working build quickly.

- Why Spring Boot? The framework provides many attributes a microservice benefits from out of the box:
    - Conventions for building, configuration, logging, and rest.
    - Is familiar to a large community of developers.
    - Adheres to many/all? principals of 12 factor apps ( https://12factor.net/ ).
    - Production "must haves" are easy: healthchecks, metrics, configuration overrides.

## Tradeoffs

I considered several options during implementation. The solution that I ended up
with is simple, has integration tests, and demonstrates the basic functionality
and layout of a solution to the problem.

I believe there are more interesting solutions that could have been produced but
they would have omitted test cases, documentation, etc. I look forward to talking
about these tradeoffs in person.

## Functionality

This is not a full implementation of the problem statement. Specifically `mentions` and `emoticons`
have not been implemented. This is largely a function of the time constraint of the
project however the outcome demonstrates my approach to building software:

- Identify areas that add the most risk to a project and address these first (in this case the `links`
because of the added network and html parsing).

- Build software in layers.

- Add complexity as it is demanded.

- Look for opportunities for asymmetric returns on investment. Spend time where it will achieve the most project value.

- Simple solutions with proven technology are often a good choice.

## Next Steps

I have many ideas for improving and maturing this code base. I am excited
to talk about them in person!

# Setup

This project uses a build common pattern for java applications.

### Requirements:

 - Java JDK > 1.8
 - Maven 3

### Build + Run tests + Create build artifact

    $ mvn clean package

## Problem Statement

Hi Ryan,
We'd like you to complete a take-home coding exercise.  This exercise is not meant to be tricky or complex; however, it does represent a typical problem faced by the HipChat Engineering team.  Here are a few things to keep in mind as you work through it:
* The position is for a role on the Platform team.  As a platform team, we work in golang.  If you are not comfortable working in golang, we encourage you to code your solution using the language of your choice. We are much more interested in how you solve the problem than we are in how much of a new language you can learn in a few hours.
* Please only spend 2 hours on this exercise.   Treat this as if you're a member of the HipChat Engineering team and are solving it as part of your responsibilities there.
* Be thorough and take the opportunity to show the HipChat Engineering team that you've got technical chops.
* Using frameworks and libraries is acceptable. We are looking for how you would solve a problem like this on the job. If that involves bringing in libraries then do so, and even better, tell us why you made the choice.

When you think it's ready for prime time, push your work to a public repo on Bitbucket or Github and send us a link.

Now, for the coding exercise...
Please write a RESTful API that takes a chat message string as input and returns a JSON object containing information about its contents as described below.

Your service should parse the following data from the input:
1. mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character. (https://confluence.atlassian.com/hipchat/get-teammates-attention-744328217.html)
2. Emoticons - For this exercise, you only need to consider 'custom' emoticons which are alphanumeric strings, no longer than 15 characters, contained in parenthesis. You can assume that anything matching this format is an emoticon. (https://www.hipchat.com/emoticons)
3. Links - Any URLs contained in the message, along with the page's title.

The response should be a JSON object containing arrays of all matches parsed from the input string.
For example, calling your function with the following inputs should result in the corresponding return values.
Input: "@chris you around?"
Return:
{
  "mentions": [
    "chris"
  ]
}

Input: "Good morning! (megusta) (coffee)"
Return:
{
  "emoticons": [
    "megusta",
    "coffee"
  ]
}

Input: "Olympics are starting soon; http://www.nbcolympics.com"
Return:
{
  "links": [
    {
      "url": "http://www.nbcolympics.com",
      "title": "2016 Rio Olympic Games | NBC Olympics"
    }
  ]
}

Input: "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
Return:
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "success"
  ],
  "links": [
    {
      "url": "https://twitter.com/jdorfman/status/430511497475670016",
      "title": "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
    }
  ]
}

Good luck!
